
# Introduction to GitLab on GKE
## Google Next 2019 Spotlight Labs

![[/fragments/labmanuallogo]]

## Outline
{:.no_toc}
- TOC
{:toc}

## Introduction

Google’s Kubernetes Engine simplifies the management and use of containers on Kubernetes. GKE makes spinning up, scaling up and down, and retiring Kubernetes clusters easier than it has ever been. This is helping to bring the use of Kubernetes to run cloud-native apps to a larger number of practitioners.

However, a Kubernetes cluster alone doesn’t make great apps. Developers do. The software that is running in these clusters needs to be imagined, created, tested, released and managed. GitLab is the single application that provides all of these capabilities out of the box, for seamless, low maintenance, just-commit-code software development, and delivery experience.

In this lab we will go through the process of creating a GKE cluster, installing a GitLab instance into it, editing a web app, and then deploying it to the GKE cluster.

### What you will build

In this lab, you are going to install GitLab from scratch into a GKE cluster, then configure and use GitLab to edit and deploy a web app to production. This lab will illustrate how easy it is to get an entire high-quality DevOps software delivery pipeline working, without the hassles of integrations, so that you can get to focus on creating great software.

### What you'll learn
In this lab you'll learn how to do the following:

* Create, connect to, and configure a Google Kubernetes Engine (GKE) Cluster
* Collect needed information about cluster details
* Install a GitLab instance into the Kubernetes cluster
* Clone a simple “Hello World”-style web app project into GitLab
* Configure the GitLab instance for Kubernetes and Auto DevOps
* Make an edit to the web app and redeploy into production
* Observe how much of the software delivery life cycle GitLab does for you, out of the box.

### What you'll need

* A recent version of  [Chrome](https://www.google.com/chrome/) is recommended
* Basic knowledge of Linux CLI and [gcloud](https://cloud.google.com/sdk/gcloud/)

This lab is focused on GKE and GitLab deployment and management. Non-relevant concepts and code blocks are glossed over and are provided for you to simply copy and paste.

## Setup

![[/fragments/startqwiklab]]

![[/fragments/gcpconsole]]

![[/fragments/cloudshell]]

### Picking a zone
To spread the load amongst the different zones within the specific region where this Qwiklab is running, execute the following command within Cloud Shell. This command will randomly select a GCP Compute zone within the us-central1 region, set it as the default zone for your Cloud Shell session, and display the zone within the Cloud Shell console for your reference:

```
python -c 'import random, os; \
l = random.choice(["a", "b", "c", "f"]); \
os.system("gcloud config set compute/zone us-central1-{}".format(l)); \
print("You will be using GCP zone: us-central1-{}".format(l))'
```

Example output:
```
Updated property [compute/zone].
You will be using GCP zone: us-central1-a
```

A full list of available GCP zones can be found [here](https://cloud.google.com/compute/docs/regions-zones/\#available).


## Create a Google Kubernetes Engine (GKE) Cluster

In this section we will create a Kubernetes cluster on GKE which we will install GitLab into, as well as deploy managed apps to.


### Create a cluster
In the Cloud Shell command line, create a cluster by executing the following command:

```
gcloud container clusters create gitlab-gke-cl --num-nodes=3 --machine-type=n1-standard-2 --node-version=1.11.7-gke.4
```
This will display several warnings which you can ignore. It may take a few minutes to complete.

You'll know it's done when it shows output similar to:
```
NAME           LOCATION       MASTER_VERSION  MASTER_IP       MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
gitlab-gke-cl  us-central1-b  1.11.7-gke.4    35.225.100.135  n1-standard-2  1.11.7-gke.4  3          RUNNING
```

### Get authentication credentials for the cluster

You need authentication credentials to interact with the cluster.

To get authentication credentials run:

```
gcloud container clusters get-credentials gitlab-gke-cl
```

Example output:
```
Fetching cluster endpoint and auth data.
kubeconfig entry generated for gitlab-gke-cl.
```

## Install Helm

Helm is a Kubernetes package manager that helps you manage Kubernetes applications. We will use it in this lab to install GitLab onto our cluster.

Download and install Helm client:

```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
```

To give the Tiller the permissions it will need to run on the cluster we will make a `serviceaccount` for it:

```
kubectl create serviceaccount -n kube-system tiller
```

Then we will bind the `serviceaccount` to the **cluster-admin** role:
```
kubectl create clusterrolebinding tiller-binding \
   --clusterrole=cluster-admin \
   --serviceaccount kube-system:tiller
```

And now we can init Helm using our new priveleged `serviceaccount`
```
helm init --service-account tiller
```

Finally, add the official GitLab helm repo:
```
helm repo add gitlab https://charts.gitlab.io/
helm repo update
```

Expected output on success:
```
Update Complete. ⎈ Happy Helming!⎈
```

## Get Your Inputs Ready

You will need a few pieces of information to plug in to the helm installation command

### Allocate a Static IP address

Allocate a static IP in the GCP region our cluster resides using the following command:
```
gcloud compute addresses create endpoints-ip --region us-central1
```

Verify an address is allocated:

```
gcloud compute addresses list
```

The IP address in the output is the allocated IP address.

![get ip address](images/create_cluster/get_ip_address.png)

Store this IP address in an environment variable to be used in the next step.

```
export LB_IP_ADDR="$(gcloud compute addresses list | awk '/endpoints-ip/ {print$2}')"
```

### Set your email addresses

A valid email address is needed so that Let's Encrypt can create a proper certificate. If it can't then you won't even be able to login for the first time. Run the following command to set your email address in a variable.

```
export EMAIL=<YOUR_EMAIL_ADDRESS>
```
Ex. export EMAIL=joe@smith.com

## Deploy GitLab to Kubernetes Cluster

### Run the Helm command to start the installation

Now you are ready to deploy GitLab to your Kubernetes cluster. To do so, run the
following command:
```
helm upgrade --install gitlab gitlab/gitlab                    \
             --timeout 600                                     \
             --set global.hosts.externalIP=${LB_IP_ADDR}       \
             --set global.hosts.domain=${LB_IP_ADDR}.nip.io    \
             --set certmanager-issuer.email=${EMAIL}
```
This will install gitlab from the official Helm chart, using the configuration
values you provided.

### Validate GitLab Installation

After a few minutes, your GitLab instance should be up and running in your
cluster. To validate this, ensure that your pods are all up by running:

```bash
kubectl get pods
```

The output should look something like:

```
NAME                                      READY   STATUS             RESTARTS   AGE
gitlab-gitaly-0                           1/1     Running            0          1d
gitlab-gitlab-shell-cd4c66988-4vc75       1/1     Running            0          1d
gitlab-gitlab-shell-cd4c66988-r4d4p       1/1     Running            0          1d
gitlab-migrations.2-bt5wl                 0/1     Completed          0          12h
gitlab-minio-7b67585cf5-2n47r             1/1     Running            0          1d
gitlab-minio-create-buckets.2-87p9k       0/1     Completed          0          12h
gitlab-postgresql-7756f9c75f-wx6f6        1/1     Running            0          1d
gitlab-redis-554dc46b4c-n8m59             2/2     Running            0          1d
gitlab-registry-588bdf5d64-475zv          1/1     Running            0          1d
gitlab-registry-588bdf5d64-b5gk7          1/1     Running            0          1d
gitlab-sidekiq-all-in-1-96d4d5458-ks9fz   1/1     Running            0          1d
gitlab-task-runner-8c6944d6-8lwph         1/1     Running            0          1d
gitlab-unicorn-6dbb487b56-p8dzl           2/2     Running            0          1d
gitlab-unicorn-6dbb487b56-q2jxp           2/2     Running            0          1d
```

To see when GitLab is ready for you to login, run the following command:
```
kubectl get pods -l app=unicorn
```

When you can login the output should show at least 1 `gitlab-unicorn` is available in the READY column. For example:

```
NAME                              READY     STATUS    RESTARTS   AGE
gitlab-unicorn-85747856b8-qt7bq   2/2       Running   0          4m
gitlab-unicorn-85747856b8-xt4qf   2/2       Running   0          3m
```

This can take several minutes.

## Login and Configure GitLab

### Initial Login

First, let's open a browser and navigate to your new GitLab instance. Navigate to:

```
https://gitlab.<LB_IP_ADDR>.nip.io
```
Hint: If you don't remember your LB_IP_ADDR run:
```
echo $LB_IP_ADDR
```

This should bring you to the GitLab login screen.

![login screen](images/login/login_screen.png)

Login with the username `root`.

You will need to obtain the root password of your GitLab instance by running:
```
kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath={.data.password} | base64 --decode ; echo
```

Copy the output of this command (make sure to get the whole thing), and paste into the password field.

### Add a License

Since we deployed the Enterprise Edition of GitLab, we can check out some of the more advanced features if we add a license, otherwise it'll behave just like the Community Edition.

Click on the `wrench` icon towards the end of the top menu bar:

![wrench icon](images/login/wrench_icon.png)

This will bring you to the admin area. Click on the `License` in the
left-hand sidebar:

![license icon](images/login/license_menu.png)

Click on the `Upload New License` button in the top right corner:

![upload license button](images/login/upload_license_button.png)

Ensure the radio button next to `Enter License Key` is selected:

![license config](images/login/license_config.png)

Then run the following command to output your trial license and copy it to the `License Key` text field:

```
echo; curl https://gitlab.com/dangordon/workshop-gke/raw/master/content/gitlab-on-gke/gitlab.license; echo; echo
```

Click the blue `Upload License` button:

![license config](images/login/license_submit.png)

If it worked you should see the following message:

![license success](images/login/license_success.png)

You're done. Go back to the home screen by clicking on the GitLab logo in the top left corner of the window.

![click to home](images/login/click_to_home.png)

### Create a New User

Click on the `Add people` rectangle on the GitLab home screen.

![add people](images/login/add_people.png)

Here, you will fill out the information for your new account on your instance.
Ensure that your user is given `Admin privileges`.

![admin rights](images/login/admin_rights.png)

When you are done, click the green `Create user` button at the bottom of the
screen.

## Setup Your Project

### Impersonate Your New User

Click on the `Impersonate` button at the top of your new user's page.

![Impersonate](images/login/impersonate.png)

Now you are masquerading as your new user.

### Migrate Repo from GitHub to GitLab

Click on the `Create a Project` rectangle.

![create project](images/migrate/create_project.png)

Click on the `Import Project` tab, then click `Repo by URL`. There is also a
button for `GitHub` which will allow you to import repositories from your public
and private GitHub repositories if you authorize it, but we'll stick with `Repo by URL`.

![import project](images/migrate/import_project.png)

Enter the following URL into the `Git Repository URL` text field:

```
https://github.com/eggshell/spring-app.git
```

Ensure the `Project name` and `Project slug` text fields read `spring-app`.
Also ensure the `Visibility Level` of the repo is set to `Public`. Finally,
click the green `Create Project` button to start the migration process.

![repo by url](images/migrate/repo_by_url.png)

### Add Kubernetes Cluster Info

When the migration process finishes, you should be redirected to your new
project's page. Click on the `Add Kubernetes Cluster` button near the top.

![add k8s cluster](images/k8s_cluster/add_k8s_cluster.png)

Click on the `Add Existing Cluster` tab. We'll need some information about your
Kubernetes cluster in order to hook it up to your repo and install some
software.

#### Name and API URL

The `Kubernetes Cluster Name` can be set to whatever you like.

To get the `API URL` run the following command:

```
kubectl cluster-info | awk '/Kubernetes master/ {print $NF}'
```
Add the resulting URL to the `API URL` field on the Kubernetes details page:

![k8s name and url](images/k8s_cluster/k8s_add_name_and_url.png)

#### CA Certificate

Get your cluster's CA Certificate by running:

```
echo; kubectl get secret $(kubectl get secrets | grep default-token| awk '{print $1}'
) -o jsonpath="{['data']['ca\.crt']}" | base64 --decode; echo; echo
```

Copy the certificate from your terminal and paste it into the `CA Certificate`
field. Male sure to copy both the `---BEGIN CERTIFICATE---` and the `---END CERTIFICATE---` lines as well.

![k8s add cert](images/k8s_cluster/k8s_add_cert.png)

#### Service Account Token

To give the GitLab associated Tiller the permissions it will need to run on the cluster we will make a `serviceaccount` for it. Do this by running:
```
kubectl create serviceaccount -n default gitlab
```

Then we will bind the serviceaccount to the cluster-admin role. Do this by running:
```
kubectl create clusterrolebinding gitlab-cluster-admin --serviceaccount default:gitlab \
--clusterrole=cluster-admin
```

And now we can get the service token to share to GitLab. Do this by running:
```
kubectl -n default get secret $(kubectl -n default get secrets| awk '/^gitlab-token/ {print $1}') -o jsonpath="{['data']['token']}" | base64 --decode; echo
```

Copy the resulting token string and paste it into the GitLab Kubernetes setup form under `Token`. BE CAREFUL. Some editors insert spaces or break the lines. If it doesn't work (Helm Tiller install fails with a 401 error) then double check your token.

#### The Rest

Leave the `Project Namespace` text field blank. Ensure `RBAC-enabled cluster` is checked, then click the green `Add Kubernetes Cluster` button:

![cluster config](images/k8s_cluster/k8s_add_token_rbac_save.png)

### Install Tiller and Other Apps in your Cluster

We will need to install Helm Tiller into your cluster so that GitLab can use it to manage application deployments. GitLab makes this very easy! All you have to do is click the `Install` button in the `Helm Tiller`
section.

![install tiller](images/k8s_cluster/install_tiller.png)

Wait a few moments for the installation to complete. You will know the install
was successfull when the `Install` button stops spinning and changes to
`Installed`.

<details>
<summary>Troubleshooting</summary>

**Problem:** If your Tiller installation fails with a `401 error code` - Check that your token copied and pasted ok. 401 means the token was not good. Sometimes copying it out of a Web interface/terminal will add newlines and/or spaces. Copy the token out of the terminal, and paste it into a text editor to check that there are no returns/newlines or spaces added. Then copy back out of the text editor, expand `Kubernetes cluster details`, and paste it into the `Token` field again. Then `Save` and try to re-install Tiller.

**Problem:** If your Tiller installation fails with a `403 error code` - Check that the service account the token is for has proper cluster-admin permissions and that you are referencing the service account from the right namespace. These should be correct if you copy and pasted the provided command lines for this.
</details>

Next, click the `Install` button for each of the following apps (these can all be installed in parallel):

* Ingress
* Prometheus
* GitLab Runner

![install software](images/k8s_cluster/install_software.png)


#### Verify Installation of Tiller and Other Software

Once the apps are done installing their buttons will all say `Installed` and the `Ingress IP Address` should be filled in:

![k8s apps installed](images/k8s_cluster/k8s_apps_installed.png)


Let's go take a closer look to verify that the software was installed into our Kubernetes
cluster by running the following:

```bash
kubectl get namespaces
```

In the output, you will notice that there is now a namespace called `gitlab-managed-apps`.
See what resources are in that namespace by running:

```bash
kubectl get pods --namespace gitlab-managed-apps
```

You will notice there are pods for the following apps:

* Tiller
* Ingress
* Prometheus
* Gitlab Runner

This means your GitLab instance can successfully talk back to the Kubernetes
cluster it is hosted on! Congratulations!

### Configure the domain name associated with your cluster

For Auto DevOps to work we need to assign a valid domain name to you cluster. This domain name should be set to resolve to the external IP address on your just installed Ingress service.

Copy the generated external IP address:

![k8s ingress IP](images/k8s_cluster/k8s_ingress_IP.png)

Then add it at the top of the cluster page under `Base domain` with ".nip.io" appended to the IP address and click teh `Save Changes` button. Like earlier, this will use the nip.io dynamic dns service to generate a usable public domain name for your deployed apps:

![k8s add domain](images/k8s_cluster/k8s_add_domain.png)

### Review Auto DevOps (CI/CD pipeline) configuration

GitLab provides an out-of-the-box end-to-end automatic CI/CD pipeline called Auto DevOps. Auto DevOps will build, test, secure, deploy, and monitor your apps for you. This is enabled by default, set to deploy to production at the end of the pipeline. Let's go take a quick look at that configuration.

Start by mousing over the `Settings` menu item on the bottom of the left control bar to get the `Settings` menu to come up (but don't click it). Then slide over to click on the `CI/CD` menu option.

![Go to CI/CD configuration](images/configure/go_cicd_config.png)

Next, click the `Expand` button on the `Auto DevOps` section:

![Auto DevOps section](images/configure/autodevops_section.png)

Here we can see how AutoDevOps is configured by default:

![Auto DevOps settings](images/configure/autodevops_settings.png)

Make sure that `Default to Auto DevOps pipeline` is checked and that `Continuous deployment to production` is selected. Then click on the green `Save changes` button.

That's it! GitLab has probably already kicked off a pipeline to build, test, secure, deploy, and monitor your app for you. Let's go take a look under pipelines to confirm.

Mouse over the `CI/CD` menu item on the left control bar (but don't click it). Then slide over to click on the `Pipelines` menu option.

![Go to Pipelines screen](images/configure/go_to_pipelines.png)

Here you can see that GitLab has kicked off the first pipeline on the project code. It will now do this automatically for all your commits to this project.

![Pipelines view](images/configure/pipelines_view.png)

## Let's Change Some Code

Let's now go and make a small change to the project we imported so we can see what together GitLab and Google's Kubernetes Service can do for us.

### Launch the editor
Start by going to the file repository.

![Go to files](images/create/go_to_files.png)

We'll do our editing in the built-in web IDE by clicking on the `Web IDE` button in the upper right of the window. You can of course use CLI git or any other git compatible tooling, but we'll keep it simple for today.

![Launch Web IDE](images/create/launch_web_ide.png)

### Edit the code
In the Web IDE, on the left side file tree, navigate down to `HelloController.java` and click on it to view and edit. (`src` -> `main` -> `java` -> `hello` -> `HelloController.java`)

![Navigate to file](images/create/webide_file_nav.png)

Make 4 small edits to this file:

1. Change the background color to `#6b4fbb` by replacing the highlighted text.
   ![File change background color](images/create/file_change_bg.png)

1. Edit the message and put whatever you want (suggestion: replace `Spring Boot` with `GitLab!!`.)
   ![File change message](images/create/file_change_message.png)

1. Add a graphic to the message body by pasting the following code after `line 16`:
   ```
   message += "<p><center><img src='https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png' alt='Tanuki' width='250'></center>";
   ```
   ![File change add to message](images/create/file_change_message_add.png)

1. Remove the `TODO` comment line (delete all of line 15)
![File change todo delete](images/create/file_change_todo_delete.png)

### Commit the changes
When you are done editing, your file should look as pictured below. Click on the blue `Commit...`button.

![Commit changes](images/create/webide_commit.png)

Add a short commit message, click the option to `Create a new branch and merge request` (accept the default branch name), and then click on the green `Stage & Commit` button.

![Stage and commit changes](images/create/webide_commit_2.png)

GitLab will now lead you to create the Merge Request, which is how it tracks all of the details around your submitted changes. Leave all the default input on the top of the screen. . .

![Merge request top](images/create/mr_top.png)

. . . and scroll to the bottom. On the bottom check the box to have GitLab `Remove source branch when merge request is accepted` so GitLab cleans up after us when we're done. Then click on the green `Submit merge request` button.


![Merge request top](images/create/mr_submit.png)


## Deliver the changes

GitLab automatically kicks off a pipeline to build your code changes, test them, and then deploy them to a personal review environment (called a Review App) where you and other stakeholders can validate your changes. Behind the scenes GitLab and Google Kubernetes Services are orchestrating to not only scale up the runners during the test stages, but also to spin up (and eventually down) the review environments.

### Look at the running pipeline

Click on the `pie chart` or the `pipeline ID` in the second row to get a closer look at the pipeline.

![MR with running pipeline](images/deliver/mr_running_pipeline.png)

Looking at the pipeline you can see all the tests (security and otherwise) that GitLab is running in parallel, as well as the review app setup and further testing. Remember, we didn't configure any pipelines and there was no pipeline defined as code in our repo. This is all out of the box behavior.

![pipeline graph](images/deliver/pipeline_graph.png)

The pipeline will take several minutes to complete. Once a job is running you can click into it to see live output and get more information about it. Go ahead and look into the details of a job.

![goto job details](images/deliver/goto_job_details.png)

Click on the `!1` link to bring you back to the merge request that this pipeline was launched from.

![job details](images/deliver/job_details.png)

### Review your changes

Once back at the Merge Request click on the `View app` button to take a look at the running code changes.

![MR ready to merge](images/deliver/mr_ready_to_merge.png)

Remember, this is your code change running from your branch. Nothing has been pushed to the main branch yet. Once you've confirmed your code changes resulted in the changes you wanted, you can close this window/tab and go back to the Merge Request.

![Review app](images/deliver/review_app.png)

### Merge the changes and send it to production

Once back on the merge request, you can look at the other test results, and decide if you want to merge the changes. If you do, then click on the green `Merge` button.

![Merge it!](images/deliver/mr_merge_it.png)

GitLab will complete the merge, fire off another pipeline to deliver the changes to production, and then clean up the completed branches and Kubernetes resources.

<H3>**Congratulations. You've delivered your change to production!**</H3>

<P>   
<P>   

## Next steps /learn more

We have only scratched the surface here, but this hopefully provides you a solid
base to explore GitLab and Google Kubernetes Engine further.

### Explore more of GitLab

Sign up for a [free 30-day trial license](http://bit.ly/gitlab_free_trial) (SaaS offering included).

GitLab is a single application for the entire software development life cycle. It offers
functionality from planning to creation, testing to packaging, deploying,
securing, and monitoring, allowing you to execute your entire workflow within
it's single interface. GitLab runs basically anywhere you want, from on-premise
bare metal all the way up to managed Kubernetes offerings.

### Finish your Quest
This self-paced lab is part of the Qwiklabs [Kubernetes Solutions](https://google.qwiklabs.com/quests/45) and [Website and Web Applications](https://google.qwiklabs.com/quests/39) Quests. A Quest is a series of related labs that form a learning path. Completing this Quest earns you the badge above, to recognize your achievement. You can make your badge (or badges) public and link to them in your online resume or social media account. Enroll in a Quest and get immediate completion credit if you've taken this lab. See [other available Qwiklabs Quests](https://google.qwiklabs.com/catalog).

### Take your next lab

Learn more about GKE with  [NGINX Ingress Controller on Google Kubernetes Engine](https://google.qwiklabs.com/catalog\_lab/910) or  [Running Dedicated Game Servers in Google Kubernetes Engine](https://google.qwiklabs.com/catalog\_lab/764).

Learn more about Kubernetes by taking a Quest. For the advanced user, try  [Kubernetes in the Google Cloud](https://google.qwiklabs.com/quests/29). For the expert user, try  [Kubernetes Solutions](https://google.qwiklabs.com/quests/45).

### Resources

Google GKE  
* [Google GKE Overview](https://cloud.google.com/kubernetes-engine/)  
* [Kubernetes Engine Documentation](https://cloud.google.com/kubernetes-engine/docs/)  
* [Production-Grade Container Orchestration](https://kubernetes.io/)  
* [Kubernetes The Easy Way! (For Developers In 2018)](https://youtu.be/kOa\_llowQ1c)  

Gitlab  
* [Get a free 30 day GitLab Ultimate trial license](https://about.gitlab.com/free-trial/)  
* [GitLab Auto DevOps Docs](https://docs.gitlab.com/ee/topics/autodevops/)  
* [Docs on Customizing Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/#customizing)  


##### Lab last updated March 20, 2019
